<?php
	session_start();
	$domain="/admin/";
	if(isset($_COOKIE["chupachups"])) {
		header("location:index.php");
	}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Selle WARRIORS ADMIN | Log In</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo $domain; ?>css/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $domain; ?>css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo $domain; ?>plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo" style="margin-bottom:0px;">
        <img src="img/logo.png" width="100%" />
      </div><!-- /.login-logo -->
	  <?php
          if(isset($_GET["l"]) && $_GET["l"]==1){				
      ?>
          <div class="col-xs-12 nopadding">
              <div class="alert alert-danger text-center" role="alert">
              <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  Attenzione!<br />Credenziali errate!
              </div>
          </div>
      <?php		
          }
      ?>
      <?php
          if(isset($_GET["l"]) && $_GET["l"]==2){				
      ?>
          <div class="col-xs-12 nopadding">
              <div class="alert alert-warning text-center" role="alert">
              <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  Attenzione!<br />Effettuare prima l'accesso!
              </div>
          </div>
      <?php		
          }
      ?>
      <div class="login-box-body">
        <p class="login-box-msg">Effettua l'accesso</p>
        <form role="form" action="<?php echo $domain; ?>inc/check_login.php" method="post">
		  <?php if(isset($_GET["redirect"])){ ?><input type="hidden" name="redirect" id="redirect" value="<?php echo $_GET["redirect"]; ?>" /> <?php } ?>
          <div class="form-group has-feedback">
            <input type="email" class="form-control" placeholder="Email" name="user">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Password" name="pass">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Accedi</button>
            </div><!-- /.col -->
          </div>
        </form>

        <?php /*?><div class="social-auth-links text-center">
          <p>- OR -</p>
          <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using Facebook</a>
          <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using Google+</a>
        </div><!-- /.social-auth-links -->

        <a href="#">I forgot my password</a><br>
        <a href="register.html" class="text-center">Register a new membership</a><?php */?>

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo $domain; ?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo $domain; ?>css/bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo $domain; ?>plugins/iCheck/icheck.min.js"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>
