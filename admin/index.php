<?php
	include("inc/sys2.php");
	$page="dashboard";
	$title_page="Dashboard";
?>
<!DOCTYPE html>
<html>
  <head>
    <?php include("inc/head.php"); ?>
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
    	<?php include("inc/header.php"); ?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php include("inc/aside_left.php"); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 style="text-transform:uppercase;">
            <b>&nbsp;<?php if(isset($_GET["p"])){ echo $title_profile; } ?></b>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
             <?php if(isset($_GET["p"])){?>
            <li class="active"><?php echo $title_profile; ?></li>
            <? } ?>
          </ol>
          <div class="clearfix visible-sm-block"></div>
        </section>
		<div class="clearfix visible-sm-block"></div>
        <!-- Main content -->
        <section class="content">
		<?php include("inc/info_boxes.php"); ?>
        <div class="box box-success">
            <?php /*?><div class="box-header with-border">
              <h3 class="box-title">Andamento registrazioni</h3>
            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="barChart" style="height:400px"></canvas>
              </div>
            </div>
            <!-- /.box-body -->
          </div><?php */?>
        
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <?php include("inc/footer.php"); ?>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo $domain; ?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo $domain; ?>css/bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo $domain; ?>plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo $domain; ?>js/app.min.js"></script>
   <?php /*?> <script src="<?php echo $domain; ?>plugins/chartjs/Chart.min.js"></script><?php */?>
    <!-- Sparkline -->
    <?php /*?><script src="<?php echo $domain; ?>plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="<?php echo $domain; ?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?php echo $domain; ?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="<?php echo $domain; ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="<?php echo $domain; ?>plugins/chartjs/Chart.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script>
	<?php
		$sql = "SELECT COUNT(citta) as conteggio, citta FROM track_home WHERE citta<>'-' AND citta<>'' GROUP BY citta ORDER BY conteggio DESC";
		$result = mysql_query($sql);
	?>
		var areaChartData = {
		  labels: [<?php
		  	$count_citta = 0;
			$num_citta = mysql_num_rows($result);
			while ($row = mysql_fetch_assoc($result)) {
				$chart[]=$row["conteggio"];
				$cities[]=$row["citta"];
			?>"<?php echo $row["citta"]; ?>"<?php if($count_citta<$num_citta-1){?>,<?php } $count_citta = $count_citta + 1;
			}
			$chart = implode(',', $chart);
			$chart = str_replace(",", "','", $chart);
			?>],
		  datasets: [				
			  {
				label: "Prova",
				fillColor: "rgba(210, 214, 222, 1)",
				strokeColor: "rgba(210, 214, 222, 1)",
				pointColor: "rgba(210, 214, 222, 1)",
				pointStrokeColor: "#c1c7d1",
				pointHighlightFill: "#fff",
				pointHighlightStroke: "rgba(220,220,220,1)",
				data: ['<?php echo $chart; ?>']
			  } 
		  ]
		};
	
	</script>
    
	<script src="<?php echo $domain; ?>js/pages/dashboard2.js"></script><?php */?>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo $domain; ?>js/demo.js"></script>
    
    
    <?php /*?><script>
	  $(function () {
		  
		  var areaChartData = {
		  labels: ["8 Feb", "9 Feb", "10 Feb", "11 Feb", "12 Feb", "13 Feb", "14 Feb"],
		  datasets: [
			{
			  fillColor: "#00a65a",
			  strokeColor: "#00a65a",
			  pointColor: "#00a65a",
			  pointStrokeColor: "#c1c7d1",
			  pointHighlightFill: "#fff",
			  pointHighlightStroke: "rgba(220,220,220,1)",
			 <?php
			  $sql = "SELECT DISTINCT(date(adesso)) as data_big, count(*) as quanti_big FROM tickets GROUP BY data_big ORDER BY data_big ASC";
			  $result = mysql_query($sql);
			  while ($row = mysql_fetch_assoc($result)) {
				  $chart[]=$row["quanti_big"];
			  }
			  $chart = implode(',', $chart);
			  $chart = str_replace(",", "','", $chart);
			   ?>
			  data: ['<?php echo $chart; ?>']
			}
		  ]
		};
		//-------------
		//- BAR CHART -
		//-------------
		var barChartCanvas = $("#barChart").get(0).getContext("2d");
		var barChart = new Chart(barChartCanvas);
		var barChartData = areaChartData;
		var barChartOptions = {
		  //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
		  scaleBeginAtZero: true,
		  //Boolean - Whether grid lines are shown across the chart
		  scaleShowGridLines: true,
		  //String - Colour of the grid lines
		  scaleGridLineColor: "rgba(0,0,0,.05)",
		  //Number - Width of the grid lines
		  scaleGridLineWidth: 1,
		  //Boolean - Whether to show horizontal lines (except X axis)
		  scaleShowHorizontalLines: true,
		  //Boolean - Whether to show vertical lines (except Y axis)
		  scaleShowVerticalLines: true,
		  //Boolean - If there is a stroke on each bar
		  barShowStroke: true,
		  //Number - Pixel width of the bar stroke
		  barStrokeWidth: 2,
		  //Number - Spacing between each of the X value sets
		  barValueSpacing: 5,
		  //Number - Spacing between data sets within X values
		  barDatasetSpacing: 1,
		  //String - A legend template
		  legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
		  responsive: true,
		  maintainAspectRatio: true
		};
	
		barChartOptions.datasetFill = false;
		barChart.Bar(barChartData, barChartOptions);
	  });
	</script><?php */?>
    
  </body>
</html>
