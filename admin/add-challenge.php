<?php
	include("inc/sys2.php");
	$page="add-challenge";
	$title_page="New Challenge";
	
	
	if(isset($_POST['invio'])){
	
    $name_challenge= mysql_escape_string($_POST['name_challenge']);
    $hashtag_challenge=$_POST['hashtag_challenge'];
    $description_challenge=mysql_escape_string($_POST['description_challenge']);
    $points_challenge=$_POST['points_challenge'];
    $country=3;
    $cover_challenge=$_FILES["cover_challenge"]["name"];
    $icon_challenge=$_FILES["icon_challenge"]["name"];
    
    $oggi=date('Y-m-d h:m:s');
    $query_ins="insert into sfide (sfida,data_i,id_country,testo,points,hashtag,cover,icon) values ('$name_challenge','$oggi','$country','$description_challenge','$points_challenge','$hashtag_challenge','$cover_challenge','$icon_challenge')";
    $res_ins=mysql_query($query_ins);
    $id_challenge=mysql_insert_id();
   
   
    $sel_dir="select country from countries where id=$country";
    $res_dir=mysql_query($sel_dir);
    $dir=mysql_fetch_array($res_dir);
    $dir=$dir['country'];
   
   
    if(!empty($cover_challenge)){
        $intDir="../img/challenges/cover/$dir/";


          

          $intDir2=$intDir.$id_challenge.".jpg";
          $cover_challenge_tmp=$_FILES["cover_challenge"]["tmp_name"];
          if(!move_uploaded_file($cover_challenge_tmp,$intDir2)){ echo"ERROR"; exit();}
          else chmod($intDir,0777); 
    }
    
    
    
    
       if(!empty($icon_challenge)){
        $intDir2="../img/challenges/icon/$dir/";



          $intDir3=$intDir2.$id_challenge.".jpg";
          $icon_challenge_tmp=$_FILES["icon_challenge"]["tmp_name"];
          if(!move_uploaded_file($icon_challenge_tmp,$intDir3)){ echo"ERROR"; exit();}
          else chmod($intDir2,0777); 
    } 
    
    
   
   
?>	
<script>
	alert('Sfida inserita con successo');
</script>	

<?php	
	}
	
	$q_country="select * from countries";
	$r_country=mysql_query($q_country);
	
?>
<!DOCTYPE html>
<html>
  <head>
    <?php include("inc/head.php"); ?>
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
    	<?php include("inc/header.php"); ?> 
        <?php include("inc/aside_left.php"); ?>      
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content-header">
          <h1 style="text-transform:uppercase;">
            <b><?php echo $title_page; ?></b>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo $domain; ?>index.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><?php echo $title_page; ?></li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12">
            	<div class="box box-primary"  style="border-top:none;">
                	<div class="box box-info box-solid">
                    	<div class="box-body">
                            <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
                              <input type ="hidden" name="invio" value="1">
                              <div class="form-group">
                                <label for="name_challenge">Name</label>
                                <input type="text" class="form-control" id="name_challenge" name="name_challenge">
                              </div>
                              <div class="form-group">
                                <label for="hashtag_challenge">Hashtag</label>
                                <input type="text" class="form-control" id="hashtag_challenge" name="hashtag_challenge">
                              </div>
                              <div class="form-group">
                                <label for="description_challenge">Description</label>
                                <textarea class="form-control" id="description_challenge" name="description_challenge"></textarea>
                              </div>
                              <div class="form-group">
                                <label for="points_challenge">Points</label>
                                <input type="text" class="form-control" id="points_challenge" name="points_challenge">
                              </div>
                              <?php
                              /* ?>
                              <div class="form-group">
                              <label for="points_challenge">Country</label>
                                 <select name="country">
                                 <?php while($country=mysql_fetch_array($r_country)){ ?>
                                  <option value="<?php echo $country['id']; ?>"><?php echo $country['country']; ?></option>
                                 <?php }?>
                                 </select>
                              </div>
                              <?php*/
                              ?>
                              <div class="form-group">
                                <label for="cover_challenge">Cover</label>
                                <input type="file" id="cover_challenge" name="cover_challenge">
                                <p class="help-block"> size 1000x624 pixel, square ratio</p>
                              </div>
                              <div class="form-group">
                                <label for="icon_challenge">Icon</label>
                                <input type="file" id="icon_challenge" name="icon_challenge">
                                <p class="help-block"> size 400x400 pixel, square ratio</p>
                              </div>                              
                              <button type="submit" class="btn btn-primary">Create Challenge</button>
                            </form>
                         </div>
                   </div>
             </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <?php include("inc/footer.php"); ?>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo $domain; ?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo $domain; ?>css/bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo $domain; ?>plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo $domain; ?>js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo $domain; ?>js/demo.js"></script>
  </body>
</html>
