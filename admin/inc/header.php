<a href="#0" class="cd-top"><i class="fa fa-angle-up"></i></a>
<header class="main-header">
    <!-- Logo -->
    <a href="<?php echo $domain; ?>index.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="<?php echo $domain; ?>img/logo-50x50.png" width="100%" /></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="<?php echo $domain; ?>img/logo_side.png" width="100%" /></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="messages-menu">
            <a href="genera_classifica.php?id_c=3" onClick="return confirm('Are you sure?')">
              <span>UPDATE RANKING</span>
            </a>
          </li>
          <!-- Messages: style can be found in dropdown.less-->
          <li class="messages-menu">
            <a href="javascript:;" id="lista_fb" data-c="3">
              <span>RELOAD FACEBOOK</span>
            </a>
          </li>
          <li class="messages-menu">
            <a href="javascript:;" id="lista_in" data-c="3">
              <span>RELOAD INSTAGRAM</span>
            </a>
          </li>
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo $domain; ?>img/orlando-160x160.jpg" class="user-image" alt="Orlando Chiaccio">
              <span class="hidden-xs"><?php echo $nome_utente." ".$cognome_utente; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo $img_utente; ?>" class="img-circle" alt="<?php echo $nome_utente." ".$cognome_utente; ?>">
                <p>
                  <?php echo $nome_utente." ".$cognome_utente; ?>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo $domain; ?>logout.php" class="btn btn-group-justified btn-default btn-flat">Log out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>

    </nav>
  </header>