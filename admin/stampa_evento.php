<?php
	include("inc/sys2.php");
	$page="stampa";
	$title_page="Singolo Evento";
?>
<!DOCTYPE html>
<html>
  <head>
    <?php include("inc/head.php"); ?>
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
    	<?php include("inc/header.php"); ?> 
        <?php include("inc/aside_left.php"); ?>      

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
         <?php
		// CARICO TUTTE LE PRENOTAZIONI
		$sql = "SELECT evento FROM eventi WHERE id='".$_GET["id_e"]."' LIMIT 1";
		$result = mysql_query($sql);
		while ($row = mysql_fetch_assoc($result)) {
		?>
        <section class="content-header">
          <h1 style="text-transform:uppercase;">
            <b><?php echo $row["evento"]; ?></b>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo $domain; ?>index.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><?php echo $row["evento"]; ?></li>
          </ol>
        </section>
		<?php } ?>
        <!-- Main content -->
        <section class="content">
          <!-- Info boxes -->
          <div class="row">
          	<?php //include("inc/info_boxes.php"); ?>
            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>
          </div><!-- /.row -->

          <div class="row">
            <div class="col-md-12">
            	<div class="box box-primary"  style="border-top:none;">
                    <?php /*?><div class="box-header with-border">
                      <h3 class="box-title">Andamento per fasce orarie</h3>
        
                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                    </div>
                    <div class="box-body">
                      <div class="chart">
                        <canvas id="areaChart" style="height:250px"></canvas>
                      </div>
                    </div>
                    <!-- /.box-body -->
                  </div><?php */?>
            
              <?php include("inc/tabella_stampa.php"); ?>
             </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <?php include("inc/footer.php"); ?>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo $domain; ?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo $domain; ?>css/bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo $domain; ?>plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo $domain; ?>js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo $domain; ?>js/demo.js"></script>
    <?php /*?><script src="<?php echo $domain; ?>plugins/chartjs/Chart.min.js"></script>
    <script>
		  $(function () {			
			var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
			var areaChart = new Chart(areaChartCanvas);
		
			var areaChartData = {
			  labels: ["0:00", "1:00", "2:00", "3:00", "4:00", "5:00", "6:00", "7:00", "8:00", "9:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"],
			  datasets: [
				{
				  fillColor: "#00a65a",
				  strokeColor: "#00a65a",
				  pointColor: "#00a65a",
				  pointStrokeColor: "#c1c7d1",
				  pointHighlightFill: "#fff",
				  pointHighlightStroke: "#00a65a",
				  <?php
				  	$sql = "SELECT COUNT(*) as quanti, HOUR(adesso) as orario
					FROM tickets WHERE adesso LIKE '".$_GET["g"]."%'
					GROUP BY HOUR(adesso)";
					$result = mysql_query($sql);
					 while ($row = mysql_fetch_assoc($result)) {
						$chart[$row["orario"]]=$row["quanti"];
					 }
					 $ciccio = "";
					 for($i=0; $i<24; $i++){
						if(!isset($chart[$i])){
							$chart[$i]=0;
						}
						$ciccio = $ciccio.$chart[$i].",";
					 }
					 
			  		 //$chart = str_replace(",", "','", $chart);
				 ?>
				  
				  data: [<?php echo substr($ciccio, 0, -1); ?>]
				}
			  ]
			};
		
			var areaChartOptions = {
			  //Boolean - If we should show the scale at all
			  showScale: true,
			  //Boolean - Whether grid lines are shown across the chart
			  scaleShowGridLines: false,
			  //String - Colour of the grid lines
			  scaleGridLineColor: "rgba(0,0,0,.05)",
			  //Number - Width of the grid lines
			  scaleGridLineWidth: 1,
			  //Boolean - Whether to show horizontal lines (except X axis)
			  scaleShowHorizontalLines: true,
			  //Boolean - Whether to show vertical lines (except Y axis)
			  scaleShowVerticalLines: true,
			  //Boolean - Whether the line is curved between points
			  bezierCurve: true,
			  //Number - Tension of the bezier curve between points
			  bezierCurveTension: 0.3,
			  //Boolean - Whether to show a dot for each point
			  pointDot: false,
			  //Number - Radius of each point dot in pixels
			  pointDotRadius: 4,
			  //Number - Pixel width of point dot stroke
			  pointDotStrokeWidth: 1,
			  //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
			  pointHitDetectionRadius: 20,
			  //Boolean - Whether to show a stroke for datasets
			  datasetStroke: true,
			  //Number - Pixel width of dataset stroke
			  datasetStrokeWidth: 2,
			  //Boolean - Whether to fill the dataset with a color
			  datasetFill: true,
			  //String - A legend template
			  legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
			  //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
			  maintainAspectRatio: true,
			  //Boolean - whether to make the chart responsive to window resizing
			  responsive: true
			};
			
			areaChart.Line(areaChartData, areaChartOptions);
		  });
		</script><?php */?>
  </body>
</html>
