<?php
	include("inc/sys2.php");
	$page="elenco";
	$title_page="Singolo Evento";
?>
<!DOCTYPE html>
<html>
  <head>
    <?php include("inc/head.php"); ?>
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
    	<?php include("inc/header.php"); ?> 
        <?php include("inc/aside_left.php"); ?>      

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
         <?php
		// CARICO TUTTE LE PRENOTAZIONI
		$sql = "SELECT evento FROM eventi WHERE id='".$_GET["id_e"]."' LIMIT 1";
		$result = mysql_query($sql);
		while ($row = mysql_fetch_assoc($result)) {
		?>
        <section class="content-header">
          <h1 style="text-transform:uppercase;">
            <b><?php echo $row["evento"]; ?></b>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo $domain; ?>index.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><?php echo $row["evento"]; ?></li>
          </ol>
        </section>
		<?php } ?>
        <!-- Main content -->
        <section class="content">
          <!-- Info boxes -->
          <div class="row">
          	<?php //include("inc/info_boxes.php"); ?>
            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>
          </div><!-- /.row -->

          <div class="row">
            <div class="col-md-12">
            	<div class="box box-primary"  style="border-top:none;">
                    <?php /*?><div class="box-header with-border">
                      <h3 class="box-title">Andamento per fasce orarie</h3>
        
                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                    </div>
                    <div class="box-body">
                      <div class="chart">
                        <canvas id="areaChart" style="height:250px"></canvas>
                      </div>
                    </div>
                    <!-- /.box-body -->
                  </div><?php */?>
            
              <?php include("inc/tabella_prenotazioni.php"); ?>
             </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <?php include("inc/footer.php"); ?>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo $domain; ?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo $domain; ?>css/bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo $domain; ?>plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo $domain; ?>js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo $domain; ?>js/demo.js"></script>
  </body>
</html>
